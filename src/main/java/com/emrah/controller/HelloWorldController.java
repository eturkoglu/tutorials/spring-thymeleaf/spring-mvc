package com.emrah.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
final class HelloWorldController {

	@GetMapping
	public String show() {
		return "helloworld-form";
	}
	
/*
	@PostMapping
	public String process() {
		return "helloworld";
	}
*/	

/*
	@PostMapping
	public String process(HttpServletRequest request, Model model) {		
		var message = "Yo! " + request.getParameter("studentName").toUpperCase();		
		model.addAttribute("message", message);		
		return "helloworld";
	}
*/
	
	@PostMapping
	public String process(@RequestParam String name, Model model) {		
		var message = "Yo! " + name.toUpperCase();		
		model.addAttribute("message", message);		
		return "helloworld";
	}
	
}
