package com.emrah.controller;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.emrah.model.Customer;

@Controller
@RequestMapping("/customer")
final class CustomerController {
	
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		var stringTrimmerEditor = new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}

	@GetMapping
	public String show(Model model) {
		model.addAttribute("customer", new Customer());
		return "customer-form";
	}
	
	@PostMapping
	public String process(@Valid @ModelAttribute Customer customer, BindingResult bindingResult) {
		return bindingResult.hasErrors() ? "customer-form" : "customer-confirmation";
	}
	
}
