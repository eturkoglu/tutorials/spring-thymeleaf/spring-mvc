package com.emrah.util;

import java.io.PrintWriter;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ConsoleWriter {
	
	private static PrintWriter printWriter = new PrintWriter(System.out, true);
	
	public static<T> void writeln(T str) {
		printWriter.println(str);
	}
	
	public static<T> void writeln() {
		printWriter.println();
	}
	
	public static<T> void write(T str) {
		printWriter.print(str);
	}

}
