package com.emrah.model;

import java.util.LinkedHashMap;

import lombok.Data;

@Data
public class Student {

	private String firstName;
	private String lastName;
	private String country;
	private String city;
	private String team;
	private String favoriteLanguage;
	
	private String[] operatingSystems;
	
	private LinkedHashMap<String, String> cities;
	
	public Student() {
		cities = new LinkedHashMap<String, String>();
		cities.put("Ankara", "Ankara");
		cities.put("Adana", "Adana");
		cities.put("Aksaray", "Aksaray");
		cities.put("Antalya", "Antalya");
	}
}
