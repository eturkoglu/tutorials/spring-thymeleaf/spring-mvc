package com.emrah.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.emrah.validation.CourseCode;

import lombok.Data;

@Data
public class Customer {

	private String firstName;
	
	@NotBlank
	@Size(min = 1, message = "is required" )
	private String lastName;
	
	@NotNull(message = "is required")
	@Min(value = 0, message = "must be greater than or equal to zero")
	@Max(value = 10, message = "must be less than or equal to 10")
	private Integer freePasses;
	
	@Pattern(regexp = "^[a-zA-Z0-9]{5}", message = "only 5 digits/chars")
	private String postalCode;
	
	@CourseCode(value = "TOP", message = "must start with TOP")
	private String courseCode;
	
}
